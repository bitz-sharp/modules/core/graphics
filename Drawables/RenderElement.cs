﻿using System;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Interfaces;
using Bitz.Modules.Core.Foundation.Logic;
using Bitz.Modules.Core.Graphics.Canvases;

namespace Bitz.Modules.Core.Graphics.Drawables
{
    public abstract class RenderElement : BasicObject, IVisible
    {
        protected readonly RenderDataRef _RenderData = new RenderDataRef();

        protected Canvas _Canvas;

        protected RenderElement()
        {
            _RenderData.PreDrawHook = PreDraw;
            if (AssignedCanvas == null) AssignedCanvas = Injector.GetSingleton<IGraphicsService>().DefaultCanvas;
        }

        public Canvas AssignedCanvas
        {
            set
            {
                _Canvas?.RegisteredRenderDefs.Remove(_RenderData);
                _Canvas = value;
                if (value != null) _Canvas.RegisteredRenderDefs.Add(_RenderData);
            }
            get => _Canvas;
        }

        public virtual Boolean Visible
        {
            get => _RenderData.Visible;
            set => _RenderData.Visible = value;
        }

        public override void Dispose()
        {
            AssignedCanvas = null;
            base.Dispose();
        }

        protected virtual void PreDraw()
        {
        }
    }
}