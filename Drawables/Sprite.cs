﻿using System;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Interfaces;
using OpenTK;

namespace Bitz.Modules.Core.Graphics.Drawables
{
    public class Sprite : RenderElement, IPositionable2D, ISizeable2D, IRotateable2D, IScaleable2D, IColourable
    {
        protected Vector4 _Colour;
        protected Boolean _ColourDirty;

        private Boolean _FlipX;
        protected Vector2 _Origin;
        protected Vector2 _Position;

        protected Matrix4 _RotateMatrix;
        protected Boolean _RotateMatrixDirty = true;
        protected Single _Rotation;
        protected Vector2 _RotationOrigin;
        protected Vector2 _Scale = Vector2.One;

        protected Matrix4 _ScaleMatrix;

        protected Boolean _ScaleMatrixDirty = true;
        protected Vector2 _ScaleOrigin;
        protected Vector2 _Size;
        protected Texture _Texture;
        protected Boolean _TextureCoordinatesDirty;

        protected Matrix4 _TranslateMatrix;
        protected Boolean _TranslateMatrixDirty = true;


        public Sprite()
        {
            _RenderData.Verts = new Vector3[4];
            _RenderData.Indices = new UInt32[] {0, 1, 2, 0, 2, 3};
            _Colour = Vector4.One;
            _ColourDirty = true;

            _RenderData.DiffuseColours = new[] {Vector4.One, Vector4.One, Vector4.One, Vector4.One};
            _RenderData.TextureCoords = new[] {Vector2.Zero, Vector2.UnitY, Vector2.One, Vector2.UnitX};
        }

        public virtual Texture Texture
        {
            get => _Texture;
            set
            {
                if (_Texture == value) return;
                _Texture = value;
                _TextureCoordinatesDirty = true;
                _Size = _Texture.Size;
            }
        }

        public Vector2 Origin
        {
            get => _Origin;
            set
            {
                if (_Origin == value) return;
                _Origin = value;
                _TranslateMatrixDirty = true;
            }
        }

        public virtual Single PosX
        {
            get => _Position.X;
            set
            {
                if (_Position.X == value) return;
                _Position.X = value;
                _TranslateMatrixDirty = true;
            }
        }

        public virtual Single PosY
        {
            get => _Position.Y;
            set
            {
                if (_Position.Y == value) return;
                _Position.Y = value;
                _TranslateMatrixDirty = true;
            }
        }

        public virtual Boolean FlipX
        {
            get => _FlipX;
            set
            {
                if (_FlipX == value) return;
                _FlipX = value;
                _TextureCoordinatesDirty = true;
            }
        }

        public virtual Int32 ZOrder
        {
            get => _RenderData.ZOrder;
            set { _RenderData.ZOrder = value; }
        }

        public virtual Vector4 Colour
        {
            get => _Colour;
            set
            {
                if (_Colour == value) return;
                _Colour = value;
                _ColourDirty = true;
            }
        }

        public virtual Single Alpha
        {
            get => _Colour.W;
            set
            {
                if (_Colour.W == value) return;
                _Colour.W = value;
                _ColourDirty = true;
            }
        }

        public virtual Vector2 Position
        {
            get => _Position;
            set
            {
                if (_Position == value) return;
                _Position = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Single Rotation
        {
            get => _Rotation;
            set
            {
                if (_Rotation == value) return;
                _Rotation = value;
                _RotateMatrixDirty = true;
            }
        }

        public Vector2 RotationOrigin
        {
            get => _RotationOrigin;
            set
            {
                if (_RotationOrigin == value) return;
                _RotationOrigin = value;
                _RotateMatrixDirty = true;
            }
        }

        public Vector2 Scale
        {
            get => _Scale;
            set
            {
                if (_Scale == value) return;
                _Scale = value;
                _ScaleMatrixDirty = true;
            }
        }

        public Vector2 ScaleOrigin
        {
            get => _ScaleOrigin;
            set
            {
                if (_ScaleOrigin == value) return;
                _ScaleOrigin = value;
                _ScaleMatrixDirty = true;
            }
        }

        public Vector2 Size
        {
            get => _Size;
            set
            {
                if (_Size == value) return;
                _Size = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Single Width
        {
            get => _Size.X;
            set
            {
                if (_Size.X == value) return;
                _Size.X = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Single Height
        {
            get => _Size.Y;
            set
            {
                if (_Size.Y == value) return;
                _Size.Y = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Boolean RecHitTest(Vector2 position)
        {
            return position.X >= PosX && position.X <= PosX + Width && position.Y >= PosY && position.Y <= PosY + Height;
        }

        protected override void PreDraw()
        {
            if (_RotateMatrixDirty || _ColourDirty || _ScaleMatrixDirty || _TextureCoordinatesDirty || _TranslateMatrixDirty) UpdateRenderData();

            base.PreDraw();
        }

        protected virtual void UpdateRenderData()
        {
            Boolean matrixHasChanged = false;

            if (_TextureCoordinatesDirty && Texture != null)
            {
                if (_FlipX)
                    _RenderData.TextureCoords = new[]
                    {
                        new Vector2(Texture.Uvx + Texture.UvWidth, Texture.Uvy),
                        new Vector2(Texture.Uvx + Texture.UvWidth, Texture.Uvy + Texture.UvHeight),
                        new Vector2(Texture.Uvx, Texture.Uvy + Texture.UvHeight),
                        new Vector2(Texture.Uvx, Texture.Uvy)
                    };
                else
                    _RenderData.TextureCoords = new[]
                    {
                        new Vector2(Texture.Uvx, Texture.Uvy),
                        new Vector2(Texture.Uvx, Texture.Uvy + Texture.UvHeight),
                        new Vector2(Texture.Uvx + Texture.UvWidth, Texture.Uvy + Texture.UvHeight),
                        new Vector2(Texture.Uvx + Texture.UvWidth, Texture.Uvy)
                    };
                _RenderData.SourceTexture = Texture.SourceTexture;
                _TextureCoordinatesDirty = false;
            }

            if (_ColourDirty)
            {
                _ColourDirty = false;

                _RenderData.DiffuseColours[0] = _Colour;
                _RenderData.DiffuseColours[1] = _Colour;
                _RenderData.DiffuseColours[2] = _Colour;
                _RenderData.DiffuseColours[3] = _Colour;
            }

            if (_TranslateMatrixDirty)
            {
                _TranslateMatrix = Matrix4.CreateTranslation(new Vector3(_Position - _Origin));
                _TranslateMatrixDirty = false;

                matrixHasChanged = true;
            }

            if (_RotateMatrixDirty)
            {
                Vector3 rotationOrigin = new Vector3(_RotationOrigin);
                if (Math.Abs(_Rotation) < 0.01f)
                {
                    _RotateMatrix = Matrix4.Identity;
                    matrixHasChanged = true;
                }
                else
                {
                    _RotateMatrix = Matrix4.CreateTranslation(-rotationOrigin) * Matrix4.CreateRotationZ(_Rotation) * Matrix4.CreateTranslation(rotationOrigin);
                    matrixHasChanged = true;
                }

                _RotateMatrixDirty = false;
            }

            if (_ScaleMatrixDirty)
            {
                Vector3 scaleOrigin = new Vector3(_ScaleOrigin);
                _ScaleMatrix = Matrix4.CreateTranslation(-scaleOrigin) * Matrix4.CreateScale(_Scale.X, _Scale.Y, 1) * Matrix4.CreateTranslation(scaleOrigin);
                matrixHasChanged = true;
                _ScaleMatrixDirty = false;
            }

            if (matrixHasChanged)
            {
                Matrix4 matrixToApply = Matrix4.Identity;
                matrixToApply *= _ScaleMatrix;
                matrixToApply *= _RotateMatrix;
                matrixToApply *= _TranslateMatrix;

                _RenderData.Verts = new[]
                {
                    Vector4.Transform(Vector4.UnitW, matrixToApply).Xyz,
                    Vector4.Transform(Vector4.UnitY * _Size.Y + Vector4.UnitW, matrixToApply).Xyz,
                    Vector4.Transform(new Vector4(_Size.X, _Size.Y, 0, 1), matrixToApply).Xyz,
                    Vector4.Transform(Vector4.UnitX * _Size.X + Vector4.UnitW, matrixToApply).Xyz
                };

                for (Int32 i = 0; i < 4; i++) _RenderData.Verts[i].Z = 0;
            }
        }
    }
}