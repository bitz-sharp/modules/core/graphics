﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Interfaces;
using Bitz.Modules.Core.Foundation.Logic;

namespace Bitz.Modules.Core.Graphics.Drawables
{
    public class AnimSprite : Sprite, IUpdateable
    {
        public enum CompletionBehaviour
        {
            LOOP,
            PAUSE,
            REVERSE
        }

        private Single _FPS;
        private List<Action<AnimSprite>>[] _FrameActions;
        private TimeSpan _FrameChangeInterval;
        private Int32 _FrameID;
        private TimeSpan _LastFrameChange;

        private List<Texture> _Textures;

        public AnimSprite()
        {
            IGameLogicService gameLogicService = Injector.GetSingleton<IGameLogicService>();
            gameLogicService.RegisterIUpdateable(this);
            _Textures = new List<Texture>();
            _FrameActions = new List<Action<AnimSprite>>[0];
            _FPS = 60;
            _FrameChangeInterval = TimeSpan.FromSeconds(1.0 / 60.0);
            _LastFrameChange = gameLogicService.CurrentIntervalTime;
        }

        public IEnumerable<Texture> AnimationFrames
        {
            get => _Textures.AsReadOnly();
            set
            {
                _Textures = value.ToList();
                _FrameActions = new List<Action<AnimSprite>>[_Textures.Count];
                if (_Textures != null && _Textures.Count > 0) ChangeFrame(0);
            }
        }

        public Single FPS
        {
            get => _FPS;
            set
            {
                _FPS = value;
                _FrameChangeInterval = TimeSpan.FromSeconds(1.0 / _FPS);
            }
        }

        public Int32 CurrentFrame
        {
            get => _FrameID;
            set => ChangeFrame(value);
        }

        public override Texture Texture
        {
            get => base.Texture;
            set => throw new NotSupportedException("For setting textures on AnimSprites please use the AnimationTextures property");
        }

        public Action OnComplete { get; set; }

        public Action OnFrameChange { get; set; }

        public Boolean Paused { get; set; }

        public CompletionBehaviour CompleteBehaviour { get; set; }

        public Boolean TryUpdate(TimeSpan currentInstanceTime)
        {
            Int32 frameCount = (Int32) ((currentInstanceTime.Ticks - _LastFrameChange.Ticks) / _FrameChangeInterval.Ticks);

            _LastFrameChange += TimeSpan.FromTicks(_FrameChangeInterval.Ticks * frameCount);

            if (_Textures == null
                || _Textures.Count <= 0
                || frameCount == 0
                || Paused
            ) return false;

            for (Int32 i = 0; i < frameCount; i++)
            {
                Int32 frameChange = _FPS > 0 ? 1 : -1;

                if (_FrameID + frameChange >= _Textures.Count || _FrameID + frameChange < 0) Completed(ref frameChange);

                if (!Paused) ChangeFrame(_FrameID + frameChange);
            }

            return true;
        }

        private void Completed(ref Int32 frameChange)
        {
            switch (CompleteBehaviour)
            {
                case CompletionBehaviour.PAUSE:
                    Paused = true;
                    break;
                case CompletionBehaviour.LOOP:

                    break;
                case CompletionBehaviour.REVERSE:
                    frameChange = -frameChange;
                    _FPS = -_FPS;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            OnComplete?.Invoke();
        }

        private void ChangeFrame(Int32 newid)
        {
            if (_Textures == null || _Textures.Count == 0) throw new InvalidOperationException("This animation has no textures assigned so changing frame is not possible");

            _FrameID = newid % _Textures.Count;
            _Texture = _Textures[_FrameID];
            OnFrameChange?.Invoke();
            _TextureCoordinatesDirty = true;
            if (_FrameActions[_FrameID] != null)
                foreach (Action<AnimSprite> action in _FrameActions[_FrameID])
                    action.Invoke(this);
            _Size = _Texture.Size;
        }

        public override void Dispose()
        {
            _Textures.Clear();
            Injector.GetSingleton<IGameLogicService>().UnRegisterIUpdateable(this);
            base.Dispose();
        }

        public virtual void AddFrameAction(Action<AnimSprite> action, Int32 frame)
        {
            if (frame >= _Textures.Count || frame < 0) throw new ArgumentOutOfRangeException(nameof(frame));
            if (_FrameActions[frame] == null) _FrameActions[frame] = new List<Action<AnimSprite>>();
            _FrameActions[frame].Add(action);
        }

        public virtual void RemoveFrameAction(Action<AnimSprite> action, Int32 frame)
        {
            if (frame >= _Textures.Count || frame < 0) throw new ArgumentOutOfRangeException(nameof(frame));
            if (_FrameActions[frame] == null) return;
            _FrameActions[frame].Remove(action);
        }

        public virtual void RemoveFrameActions(Int32 frame)
        {
            if (frame >= _Textures.Count || frame < 0) throw new ArgumentOutOfRangeException(nameof(frame));
            if (_FrameActions[frame] == null) return;
            _FrameActions[frame] = new List<Action<AnimSprite>>();
        }

        public virtual ReadOnlyCollection<Action<AnimSprite>> GetFrameActions(Int32 frame)
        {
            if (frame >= _Textures.Count || frame < 0) throw new ArgumentOutOfRangeException(nameof(frame));
            if (_FrameActions[frame] == null) _FrameActions[frame] = new List<Action<AnimSprite>>();
            return _FrameActions[frame].AsReadOnly();
        }
    }
}