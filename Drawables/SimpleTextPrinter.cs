﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Exceptions;
using Bitz.Modules.Core.Foundation.Interfaces;
using OpenTK;

namespace Bitz.Modules.Core.Graphics.Drawables
{
    public class SimpleTextPrinter : RenderElement, IPositionable2D, ISizeable2D, IRotateable2D, IScaleable2D, IColourable
    {
        public enum HorizontalAlignment
        {
            LEFT,
            CENTER,
            RIGHT
        }

        public enum VerticalAlignment
        {
            TOP,
            CENTER,
            BOTTOM
        }

        internal static readonly Dictionary<Char, String> FontCharToSpriteMap = GenerateFontCharToSpriteMap();
        protected Single _BaseHeight;

        protected Single _BaseWidth;

        protected Vector4 _Colour = Vector4.One;
        protected List<Character> _CurrentCharacters;

        protected HorizontalAlignment _HAlignment;
        protected Vector2 _Origin;

        protected Vector2 _Position;
        protected Matrix4 _RotateMatrix;
        protected Boolean _RotateMatrixDirty = true;
        protected Single _Rotation;
        protected Vector2 _RotationOrigin;
        protected Vector2 _Scale = Vector2.One;

        protected Matrix4 _ScaleMatrix;
        protected Boolean _ScaleMatrixDirty = true;
        protected Vector2 _ScaleOrigin;
        protected Vector2 _Size;

        protected String _Text = "";

        protected Boolean _TextDirty;

        protected String _TexturesFolder;
        protected Matrix4 _TranslateMatrix;
        protected Boolean _TranslateMatrixDirty = true;
        protected VerticalAlignment _VAlignment;

        public String Text
        {
            get => _Text;
            set
            {
                if (value == _Text) return;
                _Text = value;
                _TextDirty = true;
                _TranslateMatrixDirty = true;
            }
        }

        public virtual String Font
        {
            get => _TexturesFolder;
            set
            {
                if (_TexturesFolder == value) return;
                _TexturesFolder = value;
                _TranslateMatrixDirty = true;
                _TextDirty = true;
            }
        }

        public Vector2 Origin
        {
            get => _Origin;
            set
            {
                if (Origin == value) return;
                _Origin = value;
                _TranslateMatrixDirty = true;
            }
        }

        public StyleTemplate Style
        {
            set
            {
                if (value.Colour != null) Colour = value.Colour.Value;
                if (value.Font != null) Font = value.Font;
                if (value.HAlignment != null) HAlignment = value.HAlignment.Value;
                if (value.VAlignment != null) VAlignment = value.VAlignment.Value;
                if (value.Scale != null) Scale = value.Scale.Value;
                if (value.Visible != null) Visible = value.Visible.Value;
                if (value.ZOrder != null) ZOrder = value.ZOrder.Value;
            }
        }

        public Single PosX
        {
            get => _Position.X;
            set
            {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (_Position.X == value) return;
                _Position.X = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Single PosY
        {
            get => _Position.Y;
            set
            {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (_Position.Y == value) return;
                _Position.Y = value;
                _TranslateMatrixDirty = true;
            }
        }

        public virtual Int32 ZOrder
        {
            get => _RenderData.ZOrder;
            set => _RenderData.ZOrder = value;
        }

        public HorizontalAlignment HAlignment
        {
            get => _HAlignment;
            set
            {
                if (_HAlignment == value) return;
                _HAlignment = value;
                _TranslateMatrixDirty = true;
            }
        }

        public VerticalAlignment VAlignment
        {
            get => _VAlignment;
            set
            {
                if (_VAlignment == value) return;
                _VAlignment = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Vector4 Colour
        {
            get => _Colour;
            set
            {
                _Colour = value;
                if (_CurrentCharacters == null) return;
                for (Int32 i = 0; i < _CurrentCharacters.Count; i++)
                {
                    _CurrentCharacters[i] = new Character
                    {
                        Char = _CurrentCharacters[i].Char,
                        Colours = new[] {value, value, value, value},
                        TextureCoordinates = _CurrentCharacters[i].TextureCoordinates,
                        Verts = _CurrentCharacters[i].Verts
                    };
                    if (_RenderData.DiffuseColours != null && !_TextDirty)
                    {
                        _RenderData.DiffuseColours[i * 4] = _CurrentCharacters[i].Colours[0];
                        _RenderData.DiffuseColours[i * 4 + 1] = _CurrentCharacters[i].Colours[1];
                        _RenderData.DiffuseColours[i * 4 + 2] = _CurrentCharacters[i].Colours[2];
                        _RenderData.DiffuseColours[i * 4 + 3] = _CurrentCharacters[i].Colours[3];
                    }
                }
            }
        }

        public Single Alpha
        {
            get => _Colour.W;
            set => Colour = new Vector4(_Colour.X, _Colour.Y, _Colour.Z, value);
        }

        public Vector2 Position
        {
            get => _Position;
            set
            {
                if (_Position == value) return;
                _Position = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Single Rotation
        {
            get => _Rotation;
            set
            {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (_Rotation == value) return;
                _Rotation = value;
                _RotateMatrixDirty = true;
            }
        }

        public Vector2 RotationOrigin
        {
            get => _RotationOrigin;
            set
            {
                if (_RotationOrigin == value) return;
                _RotationOrigin = value;
                _RotateMatrixDirty = true;
            }
        }

        public Vector2 Scale
        {
            get => _Scale;
            set
            {
                if (_Scale == value) return;
                _Scale = value;
                _ScaleMatrixDirty = true;
            }
        }

        public Vector2 ScaleOrigin
        {
            get => _ScaleOrigin;
            set
            {
                if (_ScaleOrigin == value) return;
                _ScaleOrigin = value;
                _ScaleMatrixDirty = true;
            }
        }

        public Vector2 Size
        {
            get => _Size;
            set
            {
                //todo Return correct size
                if (_Size == value) return;
                _Size = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Single Width
        {
            get => _Size.X;
            set
            {
                //TODO: Return correct size
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (_Size.X == value) return;
                _Size.X = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Single Height
        {
            get => _Size.Y;
            set
            {
                //TODO: Return correct size
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (_Size.Y == value) return;
                _Size.Y = value;
                _TranslateMatrixDirty = true;
            }
        }

        protected override void PreDraw()
        {
            if (_TextDirty) BuildTextData();
            if (_RotateMatrixDirty || _ScaleMatrixDirty || _TranslateMatrixDirty || _TranslateMatrixDirty) UpdateRenderData();
            base.PreDraw();
        }

        protected virtual void BuildTextData()
        {
            Injector.GetSingleton<ILoggerService>().Log(LogSeverity.DEBUG, $"{nameof(SimpleTextPrinter)}:{nameof(BuildTextData)}");
            _CurrentCharacters?.Clear();
            _CurrentCharacters = new List<Character>();
            Single xTracker = 0;
            _BaseHeight = 0;

            foreach (Char c in _Text)
            {
                if (FontCharToSpriteMap.Count == 0) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.WARNING, $"{nameof(SimpleTextPrinter)} is rebuilding its text data even though {nameof(FontCharToSpriteMap)} is empty");
                String filename = FontCharToSpriteMap.FirstOrDefault(a => a.Key.Equals(c)).Value;
                if (String.IsNullOrEmpty(filename))
                {
                    Injector.GetSingleton<ILoggerService>().Log(LogSeverity.DEBUG, $"{nameof(SimpleTextPrinter)} while rebuilding its text data encountered a missing character {c}");
                    continue;
                }

                Texture texture = Injector.GetSingleton<IAssetsService>().GetTexture($"{_TexturesFolder}\\{filename}.png");
                if (texture == null)
                {
                    Injector.GetSingleton<ILoggerService>().Log(LogSeverity.DEBUG, $"{nameof(SimpleTextPrinter)} while rebuilding its text data encountered a missing character {c}");
                    continue;
                }

                if (_RenderData.SourceTexture == null)
                {
                    _RenderData.SourceTexture = texture.SourceTexture;
                }
                else
                {
                    if (_RenderData.SourceTexture != texture.SourceTexture) throw new FontTextureLimitationException();
                }

                if (texture.Uvtl == texture.Uvbl || texture.Uvbr == texture.Uvtr) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.DEBUG, $"{nameof(SimpleTextPrinter)} Appears to be using a zero width character");

                _CurrentCharacters.Add(new Character
                {
                    Char = c,
                    TextureCoordinates = new[]
                    {
                        texture.Uvtl, texture.Uvbl, texture.Uvbr, texture.Uvtr
                    },
                    Colours = new[]
                    {
                        _Colour, _Colour, _Colour, _Colour
                    },
                    Verts = new[]
                    {
                        new Vector3(xTracker, 0, 0), new Vector3(xTracker, texture.Height, 0), new Vector3(xTracker + texture.Width, texture.Height, 0), new Vector3(xTracker + texture.Width, 0, 0)
                    }
                });
                xTracker += texture.Width;
                _BaseHeight = Math.Max(_BaseHeight, texture.Height);
            }

            _BaseWidth = xTracker;
            _TranslateMatrixDirty = true;
            _TextDirty = false;
        }

        protected virtual void UpdateRenderData()
        {
            if (_ScaleMatrixDirty)
            {
                Vector3 scaleOrigin = new Vector3(_ScaleOrigin);
                _ScaleMatrix = Matrix4.CreateTranslation(-scaleOrigin) * Matrix4.CreateScale(_Scale.X, _Scale.Y, 1) * Matrix4.CreateTranslation(scaleOrigin);
                _ScaleMatrixDirty = false;
            }

            if (_TranslateMatrixDirty)
            {
                Single actualWidth = Vector4.Transform(new Vector4(_BaseWidth, 0, 0, 1), _ScaleMatrix).X;
                Single actualHeight = Vector4.Transform(new Vector4(0, _BaseHeight, 0, 1), _ScaleMatrix).Y;

                Vector3 offest = Vector3.Zero;
                switch (_HAlignment)
                {
                    case HorizontalAlignment.LEFT:
                        break;
                    case HorizontalAlignment.CENTER:
                        offest = new Vector3(0.5f * actualWidth * Vector2.UnitX);
                        break;
                    case HorizontalAlignment.RIGHT:
                        offest = new Vector3(actualWidth * Vector2.UnitX);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                switch (_VAlignment)
                {
                    case VerticalAlignment.TOP:
                        break;
                    case VerticalAlignment.CENTER:
                        offest += new Vector3(0.5f * actualHeight * Vector2.UnitY);
                        break;
                    case VerticalAlignment.BOTTOM:
                        offest += new Vector3(actualHeight * Vector2.UnitY);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                offest = new Vector3(_Position - _Origin) - offest;
                _TranslateMatrix = Matrix4.CreateTranslation(offest);

                _TranslateMatrixDirty = false;
            }

            if (_RotateMatrixDirty)
            {
                Vector3 rotationOrigin = new Vector3(_RotationOrigin);
                if (Math.Abs(_Rotation) < 0.01f)
                    _RotateMatrix = Matrix4.Identity;
                else
                    _RotateMatrix = Matrix4.CreateTranslation(-rotationOrigin) * Matrix4.CreateRotationZ(_Rotation) * Matrix4.CreateTranslation(rotationOrigin);
                _RotateMatrixDirty = false;
            }

            Matrix4 matrixToApply = Matrix4.Identity;
            matrixToApply *= _ScaleMatrix;
            matrixToApply *= _RotateMatrix;
            matrixToApply *= _TranslateMatrix;

            _RenderData.Verts = new Vector3[_CurrentCharacters.Count * 4];
            _RenderData.DiffuseColours = new Vector4[_CurrentCharacters.Count * 4];
            _RenderData.TextureCoords = new Vector2[_CurrentCharacters.Count * 4];
            _RenderData.Indices = new UInt32[_CurrentCharacters.Count * 6];
            for (Int32 i = 0; i < _CurrentCharacters.Count; i++)
            {
                _RenderData.Verts[i * 4] = Vector4.Transform(new Vector4(_CurrentCharacters[i].Verts[0], 1), matrixToApply).Xyz;
                _RenderData.Verts[i * 4 + 1] = Vector4.Transform(new Vector4(_CurrentCharacters[i].Verts[1], 1), matrixToApply).Xyz;
                _RenderData.Verts[i * 4 + 2] = Vector4.Transform(new Vector4(_CurrentCharacters[i].Verts[2], 1), matrixToApply).Xyz;
                _RenderData.Verts[i * 4 + 3] = Vector4.Transform(new Vector4(_CurrentCharacters[i].Verts[3], 1), matrixToApply).Xyz;

                _RenderData.DiffuseColours[i * 4] = _CurrentCharacters[i].Colours[0];
                _RenderData.DiffuseColours[i * 4 + 1] = _CurrentCharacters[i].Colours[1];
                _RenderData.DiffuseColours[i * 4 + 2] = _CurrentCharacters[i].Colours[2];
                _RenderData.DiffuseColours[i * 4 + 3] = _CurrentCharacters[i].Colours[3];

                _RenderData.TextureCoords[i * 4] = _CurrentCharacters[i].TextureCoordinates[0];
                _RenderData.TextureCoords[i * 4 + 1] = _CurrentCharacters[i].TextureCoordinates[1];
                _RenderData.TextureCoords[i * 4 + 2] = _CurrentCharacters[i].TextureCoordinates[2];
                _RenderData.TextureCoords[i * 4 + 3] = _CurrentCharacters[i].TextureCoordinates[3];

                _RenderData.Indices[i * 6] = (UInt32) (i * 4);
                _RenderData.Indices[i * 6 + 1] = (UInt32) (i * 4 + 1);
                _RenderData.Indices[i * 6 + 2] = (UInt32) (i * 4 + 2);
                _RenderData.Indices[i * 6 + 3] = (UInt32) (i * 4);
                _RenderData.Indices[i * 6 + 4] = (UInt32) (i * 4 + 2);
                _RenderData.Indices[i * 6 + 5] = (UInt32) (i * 4 + 3);
            }

            for (Int32 i = 0; i < 4 * _CurrentCharacters.Count; i++) _RenderData.Verts[i].Z = 0;
        }

        private static Dictionary<Char, String> GenerateFontCharToSpriteMap()
        {
            Injector.GetSingleton<ILoggerService>().Log(LogSeverity.DEBUG, $"{nameof(SimpleTextPrinter)}:{nameof(GenerateFontCharToSpriteMap)}");
            Dictionary<Char, String> returnDict = new Dictionary<Char, String>();
            for (Int32 i = 0; i < 26; i++) returnDict.Add((Char) ('a' + i), ((Char) ('a' + i)).ToString());
            for (Int32 i = 0; i < 26; i++) returnDict.Add((Char) ('A' + i), "u" + (Char) ('A' + i));
            for (Int32 i = 0; i < 10; i++) returnDict.Add((Char) ('0' + i), ((Char) ('0' + i)).ToString());
            returnDict.Add(',', "comma");
            returnDict.Add('.', "dot");
            returnDict.Add('/', "forwardslash");
            returnDict.Add(' ', "space");
            returnDict.Add('+', "plus");
            returnDict.Add('-', "dash");
            returnDict.Add(':', "colon");
            returnDict.Add('%', "percent");
            return returnDict;
        }

        protected struct Character
        {
            public Char Char;
            public Vector3[] Verts;
            public Vector2[] TextureCoordinates;
            public Vector4[] Colours;
        }

        public struct StyleTemplate
        {
            public Vector4? Colour { get; set; }
            public HorizontalAlignment? HAlignment { get; set; }
            public VerticalAlignment? VAlignment { get; set; }
            public Vector2? Scale { get; set; }
            public String Font { get; set; }
            public Boolean? Visible { get; set; }
            public Int32? ZOrder { get; set; }
        }
    }
}