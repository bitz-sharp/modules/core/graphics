﻿#if !WEB
using System;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Interfaces;
using OpenTK;

namespace Bitz.Modules.Core.Graphics.Drawables
{
    public class Model : RenderElement, IPositionable3D, IRotateable3D, IScaleable3D, IColourable
    {
        protected Vector3 _Origin;
        protected Matrix4 _OverallMatrix = Matrix4.Identity;
        protected Vector3 _Position;
        protected Matrix4 _RotateMatrix = Matrix4.Identity;
        protected Boolean _RotateMatrixDirty;
        protected Vector3 _Rotation;
        protected Vector3 _RotationOrigin;
        protected Vector3 _Scale;
        protected Matrix4 _ScaleMatrix = Matrix4.Identity;
        protected Boolean _ScaleMatrixDirty;
        protected Vector3 _ScaleOrigin;
        protected Vector3[] _SourceNormals;
        protected Vector2[] _SourceTextureCoordinates;

        protected Vector3[] _SourceVerts;

        protected Texture _Texture;
        protected Boolean _TexutreDataDirty;

        protected Matrix4 _TranslateMatrix = Matrix4.Identity;

        protected Boolean _TranslateMatrixDirty;

        public Model(ModelData data)
        {
            Data_Verts = data.Verts;
            Data_Normals = data.Normals;
            Data_Indicies = data.Indicies;
            Data_TextureCoordinates = data.TextureCoordinates;
            Data_DiffuseColours = data.DiffuseColours;
            Data_Materials = data.Materials;
            Data_MaterialIDs = data.MaterialIDs;
        }

        protected Model()
        {
            Scale = Vector3.One;
        }

        public virtual Texture Texture
        {
            get => _Texture;
            set
            {
                _TexutreDataDirty = true;
                _Texture = value;
                _RenderData.SourceTexture = Texture.SourceTexture;
            }
        }

        public Vector3[] Data_Verts
        {
            get => _SourceVerts;
            set
            {
                _RenderData.Verts = null;
                _SourceVerts = value;
            }
        }

        public Vector3[] Data_Normals
        {
            get => _SourceNormals;
            set
            {
                _RenderData.Normals = null;
                _SourceNormals = value;
            }
        }

        public Vector4[] Data_DiffuseColours
        {
            get => _RenderData.DiffuseColours;
            set => _RenderData.DiffuseColours = value;
        }

        public Vector2[] Data_TextureCoordinates
        {
            get => _RenderData.TextureCoords;
            set
            {
                _SourceTextureCoordinates = value;
                _RenderData.TextureCoords = null;
                _TexutreDataDirty = true;
            }
        }

        public UInt32[] Data_Indicies
        {
            get => _RenderData.Indices;
            set => _RenderData.Indices = value;
        }

        public Int32[] Data_MaterialIDs
        {
            get => _RenderData.MaterialIDs;
            set => _RenderData.MaterialIDs = value;
        }

        public Material[] Data_Materials
        {
            get => _RenderData.Materials;
            set => _RenderData.Materials = value;
        }

        public Single PosX
        {
            get => _Position.X;

            set
            {
                _Position.X = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Single PosY
        {
            get => _Position.Y;

            set
            {
                _Position.Y = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Single PosZ
        {
            get => _Position.Z;

            set
            {
                _Position.Z = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Single Alpha
        {
            get { return _RenderData.DiffuseColours[0].W; }
            set
            {
                for (Int32 i = 0; i < _RenderData.DiffuseColours.Length; i++) _RenderData.DiffuseColours[i].W = value;
            }
        }

        public Vector4 Colour
        {
            get { return _RenderData.DiffuseColours[0]; }

            set
            {
                for (Int32 i = 0; i < _RenderData.DiffuseColours.Length; i++) _RenderData.DiffuseColours[i] = value;
            }
        }

        public Vector3 Position
        {
            get => _Position;

            set
            {
                _Position = value;
                _TranslateMatrixDirty = true;
            }
        }

        public Vector3 Rotation
        {
            get => _Rotation;

            set
            {
                _Rotation = value;
                _RotateMatrixDirty = true;
            }
        }

        public Vector3 RotationOrigin
        {
            get => _RotationOrigin;

            set
            {
                _RotationOrigin = value;
                _RotateMatrixDirty = true;
            }
        }

        public Vector3 Scale
        {
            get => _Scale;

            set
            {
                _Scale = value;
                _ScaleMatrixDirty = true;
            }
        }

        public Vector3 ScaleOrigin
        {
            get => _ScaleOrigin;

            set
            {
                _ScaleOrigin = value;
                _ScaleMatrixDirty = true;
            }
        }

        protected override void PreDraw()
        {
            if (_RotateMatrixDirty || _ScaleMatrixDirty || _TranslateMatrixDirty || _RenderData.Verts == null) UpdateRenderData();
            base.PreDraw();
        }

        protected virtual void UpdateRenderData()
        {
            if (_TexutreDataDirty)
            {
                Vector2 source = Texture.Uvtl;
                Vector2 multiplier = Texture.Uvbr - Texture.Uvtl;
                _RenderData.TextureCoords = new Vector2[_SourceTextureCoordinates.Length];
                for (Int32 i = 0; i < _SourceTextureCoordinates.Length; i++)
                {
                    _RenderData.TextureCoords[i] = _SourceTextureCoordinates[i] * multiplier + source;
                    _RenderData.TextureCoords[i].Y = 1 - _RenderData.TextureCoords[i].Y;
                }
            }

            if (_TranslateMatrixDirty)
            {
                _TranslateMatrix = Matrix4.CreateTranslation(new Vector3(_Position - _Origin));
                _TranslateMatrixDirty = false;
            }

            if (_RotateMatrixDirty)
            {
                Vector3 rotationOrigin = new Vector3(_RotationOrigin);

                _RotateMatrix = Matrix4.CreateTranslation(-rotationOrigin) * Matrix4.CreateRotationZ(_Rotation.Z) * Matrix4.CreateRotationY(_Rotation.Y) * Matrix4.CreateRotationY(_Rotation.X) * Matrix4.CreateTranslation(rotationOrigin);

                _RotateMatrixDirty = false;
            }

            if (_ScaleMatrixDirty)
            {
                Vector3 scaleOrigin = new Vector3(_ScaleOrigin);
                _ScaleMatrix = Matrix4.CreateTranslation(-scaleOrigin) * Matrix4.CreateScale(_Scale.X, _Scale.Y, _Scale.Z) * Matrix4.CreateTranslation(scaleOrigin);
                _ScaleMatrixDirty = false;
            }


            Matrix4 matrixToApply = Matrix4.Identity;
            matrixToApply *= _ScaleMatrix;
            matrixToApply *= _RotateMatrix;
            matrixToApply *= _TranslateMatrix;

            _RenderData.Verts = new Vector3[_SourceVerts.Length];
            if (_SourceNormals != null) _RenderData.Normals = new Vector3[_SourceNormals.Length];
            for (Int32 i = 0; i < _SourceVerts.Length; i++) _RenderData.Verts[i] = Vector4.Transform(new Vector4(_SourceVerts[i].X, _SourceVerts[i].Y, _SourceVerts[i].Z, 1), matrixToApply).Xyz;

            if (_SourceNormals != null)
                for (Int32 i = 0; i < _SourceNormals.Length; i++)
                    _RenderData.Normals[i] = Vector4.Transform(new Vector4(_SourceNormals[i].X, _SourceNormals[i].Y, _SourceNormals[i].Z, 1), _RotateMatrix).Xyz;
        }
    }
}
#endif