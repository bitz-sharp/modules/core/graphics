﻿using System;
using System.Collections.Generic;
using Bitz.Modules.Core.Foundation.Graphics;
using Bitz.Modules.Core.Foundation.Services;
using Bitz.Modules.Core.Graphics.Cameras;
using Bitz.Modules.Core.Graphics.Canvases;
using OpenTK;

namespace Bitz.Modules.Core.Graphics
{
    public interface IGraphicsService : IService
    {
        Canvas DefaultCanvas { get; }
        Single FPS { get; set; }
        Boolean Paused { get; set; }
        Vector2 Resolution { get; set; }
        IRenderSystem RenderSystem { get; set; }
        ScreenLayout RequestedScreenLayout { get; }

        IEnumerable<ICamera> GetAllCameras();
        IEnumerable<ICanvas> GetAllCanvas();
        IWindowInstance GetWindowInstance();

        void SetRequestedLayout(ScreenLayout requested);

        void RegisterCamera(ICamera camera);
        void UnRegisterCamera(ICamera camera);

        void RegisterCanvas(Canvas canvas);
        void UnRegisterCanvas(Canvas canvas);
        void InvalidateAllCameraMatrix();

        void Draw();
    }
}