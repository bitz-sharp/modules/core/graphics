﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Graphics;
using Bitz.Modules.Core.Foundation.Services;
using Bitz.Modules.Core.Graphics.Cameras;
using Bitz.Modules.Core.Graphics.Canvases;
using OpenTK;

namespace Bitz.Modules.Core.Graphics
{
    public class GraphicsService : Service, IGraphicsService
    {
        private readonly List<ICamera> _CreatedCameras = new List<ICamera>();
        private readonly Stopwatch _Stopwatch = Stopwatch.StartNew();
        private TimeSpan _DebugFrameStart;

        private List<Canvas> _KnownCanvases = new List<Canvas>();
        private TimeSpan _LastFrameInterval = TimeSpan.Zero;

        private Boolean _Paused;

        private IRenderSystem _RenderSystem;

        private ScreenLayout _RequestedScreenLayout;

        private Single _TargetFPS;
        private Double _WalkingAverageFrameDrawTimeMS;

        public ScreenLayout RequestedScreenLayout
        {
            get => _RequestedScreenLayout;
            private set
            {
                _RequestedScreenLayout = value;
                _TargetFPS = _RequestedScreenLayout.FPS;
            }
        }

        public Vector2 Resolution
        {
            get => _RenderSystem.GetWindowInstance().Resolution;
            set => _RenderSystem.SetResolution(value);
        }

        /// <summary>
        ///     Sets the FPS of the render system
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Will exception if value provided is Less than or equal to 0</exception>
        public Single FPS
        {
            get => _TargetFPS;
            set
            {
                if (value < Single.Epsilon) throw new ArgumentOutOfRangeException(nameof(value), "FPS cannot be set to 0 or a negative value");
                _TargetFPS = value;
            }
        }

        /// <summary>
        ///     A booleans that represent whether the render system is paused
        /// </summary>
        public Boolean Paused
        {
            get => _Paused;
            set => _Paused = value;
        }


        public IRenderSystem RenderSystem
        {
            get
            {
                if (_RenderSystem == null) _RenderSystem = Injector.GetSingleton<IRenderSystem>();
                return _RenderSystem;
            }
            set => _RenderSystem = value;
        }

        public Canvas DefaultCanvas
        {
            get => (Canvas) Injector.GetSingleton<ICanvas2D>();
            set { }
        }

        public virtual void Draw()
        {
            if (_RenderSystem == null)
            {
                _RenderSystem = Injector.GetSingleton<IRenderSystem>();
                _RenderSystem.VSyncActive = true;
            }

            if (_RenderSystem?.GetWindowInstance() == null || !_RenderSystem.GetWindowInstance().IsWindowReady()) return;

            if ((_Stopwatch.Elapsed - _LastFrameInterval).TotalSeconds > 1.0f / _TargetFPS)
            {
                _LastFrameInterval = _Stopwatch.Elapsed;

                if (DebugMode) _DebugFrameStart = _Stopwatch.Elapsed;

                _RenderSystem?.Draw(_KnownCanvases.Where(a => a.Visible));

                _WalkingAverageFrameDrawTimeMS = (_Stopwatch.Elapsed - _DebugFrameStart).TotalMilliseconds * 0.02f + _WalkingAverageFrameDrawTimeMS * 0.98f;
            }

            _RenderSystem?.ProcessWindowMessages();
        }

        /// <inheritdoc />
        public void SetRequestedLayout(ScreenLayout requested)
        {
            _RequestedScreenLayout = requested;
        }

        public void RegisterCamera(ICamera camera)
        {
            _CreatedCameras.Add(camera);
        }

        public void UnRegisterCamera(ICamera camera)
        {
            _CreatedCameras.Remove(camera);
        }

        public void RegisterCanvas(Canvas canvas)
        {
            _KnownCanvases.Add(canvas);
            _KnownCanvases = _KnownCanvases.OrderBy(a => a.Order).ToList();
        }

        public void UnRegisterCanvas(Canvas canvas)
        {
            _KnownCanvases.Remove(canvas);
        }

        public IEnumerable<ICanvas> GetAllCanvas()
        {
            return _KnownCanvases.AsReadOnly();
        }

        public IEnumerable<ICamera> GetAllCameras()
        {
            return _CreatedCameras.AsReadOnly();
        }

        public void InvalidateAllCameraMatrix()
        {
            foreach (ICamera createdCamera in _CreatedCameras) createdCamera.InvalidateMatrix();
        }

        public override void Initialize()
        {
        }

        public override String GetServiceDebugStatus()
        {
            return $"FT:{_WalkingAverageFrameDrawTimeMS},Scenes:{RenderSystem.DebugCountScenes},Drawables:{RenderSystem.DebugCountDrawable},Intervals:{RenderSystem.DebugCountRenterIntervals}";
        }

        public virtual IWindowInstance GetWindowInstance()
        {
            return RenderSystem.GetWindowInstance();
        }

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
        }

        protected override void Shutdown()
        {
            foreach (Canvas canvas in _KnownCanvases.ToList()) canvas.Dispose();
            _KnownCanvases.Clear();

            _RenderSystem.Dispose();
            _RenderSystem = null;
        }
    }
}