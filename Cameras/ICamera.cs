﻿using System;
using Bitz.Modules.Core.Foundation;
using OpenTK;

namespace Bitz.Modules.Core.Graphics.Cameras
{
    public interface ICamera : IInjectable, IDisposable
    {
        Single Height { get; set; }
        Matrix4 ProjectionMatrix { get; }
        Vector2 Size { get; set; }
        Matrix4 ViewMatrix { get; }
        Single Width { get; set; }

        void SetModeFree();
        void SetModeLocked();
        void SetModeTargetHeight(Single targetHeight);
        void SetModeTargetWidth(Single targetWidth);
        Vector4 UnProject(Vector2 position);

        void InvalidateMatrix();
    }
}