﻿using Bitz.Modules.Core.Foundation.Interfaces;

namespace Bitz.Modules.Core.Graphics.Cameras
{
    public interface ICamera3D : ICamera, IPositionable3D, IRotateable3D
    {
    }
}