﻿using Bitz.Modules.Core.Foundation.Interfaces;

namespace Bitz.Modules.Core.Graphics.Cameras
{
    public interface ICamera2D : ICamera, IPositionable2D, IRotateable2D
    {
    }
}