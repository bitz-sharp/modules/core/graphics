﻿using System;
using System.Collections.Generic;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Graphics.Canvases;
using OpenTK;

namespace Bitz.Modules.Core.Graphics
{
    public interface IRenderSystem : IInjectable
    {
        Int32 DebugCountDrawable { get; }
        Int32 DebugCountRenterIntervals { get; }
        Int32 DebugCountScenes { get; }
        Boolean VSyncActive { get; set; }

        void Dispose();
        IWindowInstance GetWindowInstance();
        void ProcessWindowMessages();
        void SetResolution(Vector2 newResolution);
        void Draw(IEnumerable<Canvas> canvasesToDraw);
        void UpdateViewport();
    }
}