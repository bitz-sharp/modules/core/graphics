﻿using System;
using OpenTK;

namespace Bitz.Modules.Core.Graphics.Shaders
{
    public interface IShader : IDisposable
    {
        String FragementShader { get; set; }
        Int32 ProgramID { get; }
        String VertexShader { get; set; }

        void PreDraw();
        void SetVariable(String name, Single value);
        void SetVariable(String name, Single[] value);
        void SetVariable(String name, Int32 value);
        void SetVariable(String name, Int32[] value);
        void SetVariable(String name, Matrix4 value);
        void SetVariable(String name, Vector2 value);
        void SetVariable(String name, Vector2[] value);
        void SetVariable(String name, Vector3 value);
        void SetVariable(String name, Vector3[] value);
        void SetVariable(String name, Vector4 value);
        void SetVariable(String name, Vector4[] value);
        void Bind();
        void InternalPreDraw();
    }
}