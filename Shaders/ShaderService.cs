﻿using System;
using Bitz.Modules.Core.Foundation.Services;

namespace Bitz.Modules.Core.Graphics.Shaders
{
    public class ShaderService : Service
    {
        public enum ShaderVersion
        {
            DESKTOP_400,
            MOBILE_300
        }

        public static ShaderVersion CURRENT_SHADER_VERSION;

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
        }

        public override void Initialize()
        {
            CURRENT_SHADER_VERSION = ShaderVersion.DESKTOP_400;

            //TODO set the mobile shader version if required   CURRENT_SHADER_VERSION = ShaderVersion.MOBILE_300;
        }

        protected override void Shutdown()
        {
        }

        public override String GetServiceDebugStatus()
        {
            return "";
        }
    }
}