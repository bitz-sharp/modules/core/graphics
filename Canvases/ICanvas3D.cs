﻿using Bitz.Modules.Core.Foundation;

namespace Bitz.Modules.Core.Graphics.Canvases
{
    public interface ICanvas3D : ICanvas, IInjectable
    {
    }
}