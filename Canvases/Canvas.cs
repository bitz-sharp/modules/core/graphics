﻿using System;
using System.Collections.Generic;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Interfaces;
using Bitz.Modules.Core.Foundation.Logic;
using Bitz.Modules.Core.Graphics.Cameras;
using Bitz.Modules.Core.Graphics.Shaders;
using OpenTK;

namespace Bitz.Modules.Core.Graphics.Canvases
{
    public abstract class Canvas : BasicObject, IVisible, ICanvas
    {
        public readonly List<RenderDataRef> RegisteredRenderDefs = new List<RenderDataRef>();

        public Action PostDraw;

        public Action PreDraw;

        protected Canvas(ICamera primaryCamera, IShader defaultShader)
        {
            PrimaryCamera = primaryCamera;
            DefaultShader = defaultShader;
            AddChild(primaryCamera);
            AddChild(defaultShader);
            Injector.GetSingleton<IGraphicsService>().RegisterCanvas(this);
        }

        /// <summary>
        ///     The camera to be used for this canvas, this belongs to the Canvas and will be disposed by it
        /// </summary>
        public virtual ICamera PrimaryCamera { get; }

        public virtual Int32 Order { get; set; }

        /// <summary>
        ///     The Default shader used by all renderables attached to this canvas, this belongs to the Canvas and will be disposed
        ///     by it
        /// </summary>
        public virtual IShader DefaultShader { get; }

        public override void Dispose()
        {
            PreDraw = null;
            PostDraw = null;

            Injector.GetSingleton<IGraphicsService>().UnRegisterCanvas(this);
        }

        public virtual Vector2 ScreenSpaceToWorldSpace(Vector2 position)
        {
            return PrimaryCamera.UnProject(position).Xy;
        }

        public abstract void ConfigureGlState();
        public virtual Boolean Visible { get; set; }
    }
}