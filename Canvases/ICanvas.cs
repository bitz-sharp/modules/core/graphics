﻿using System;
using Bitz.Modules.Core.Graphics.Cameras;
using Bitz.Modules.Core.Graphics.Shaders;
using OpenTK;

namespace Bitz.Modules.Core.Graphics.Canvases
{
    public interface ICanvas
    {
        ICamera PrimaryCamera { get; }
        IShader DefaultShader { get; }
        Int32 Order { get; set; }
        Boolean Visible { get; set; }

        void ConfigureGlState();
        void Dispose();
        Vector2 ScreenSpaceToWorldSpace(Vector2 position);
    }
}