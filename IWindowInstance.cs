﻿using System;
using Bitz.Modules.Core.Foundation;
using OpenTK;

namespace Bitz.Modules.Core.Graphics
{
    public interface IWindowInstance : IInjectable, IDisposable
    {
        Boolean Focused { get; }
        Vector2 Resolution { get; set; }
        Vector2 GetWindowDimentions();
        Boolean IsWindowReady();
        void ProcessWindowsEvents();
        void SetFullScreen(Boolean active);
        void SetSwapInterval(Int32 newInterval);
        void SetVisible(Boolean visible);
        void SwapBuffers();
    }
}