﻿using System;
using System.Linq;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Interfaces;
using Bitz.Modules.Core.Foundation.Logic;
using OpenTK;

namespace Bitz.Modules.Core.Graphics
{
    public class RenderDataRef : BasicObject, IVisible
    {
        protected Int32 _ZOrder;

        public Action PreDrawHook;

        public Int32 ZOrder { get; set; }

        public Vector3[] Verts { get; set; }

        public UInt32[] Indices { get; set; }

        public UInt32[] OffsetIndicies { get; private set; }

        public Vector4[] DiffuseColours { get; set; }

        public Vector2[] TextureCoords { get; set; }

        public Vector3[] Normals { get; set; }

        public Int32[] MaterialIDs { get; set; }

        public Material[] Materials { get; set; }

        public SourceTexture SourceTexture { get; set; }

        public Boolean ClipperActive { get; set; }

        public Vector4 ClipperBounds { get; set; }

        public Boolean Visible { get; set; } = true;

        public RenderDataRef UpdateRenderData(IRenderSystem renderSystem, ref Int32 vertPos, ref Vector3[] verts, ref Int32 diffuseColoursPos, ref Vector4[] diffuseColours, ref Int32 tcPos, ref Vector2[] tc, ref Int32 normalPos, ref Vector3[] normals, ref Int32 materialIDPos, ref Int32[] materialIDs)
        {
            if (!Visible) return null;

            if (Verts.Length != DiffuseColours.Length || Verts.Length != TextureCoords.Length) throw new UnbalancedRenderableExcaption();

            Int32 vertStartPos = vertPos;

            CheckAndExpandArray(ref verts, vertPos, Verts.Length);

            Array.Copy(Verts, 0, verts, vertPos, Verts.Length);
            vertPos += Verts.Length;

            OffsetIndicies = Indices.ToArray();
            for (Int32 i = 0; i < OffsetIndicies.Length; i++) OffsetIndicies[i] += (UInt32) vertStartPos;

            CheckAndExpandArray(ref diffuseColours, diffuseColoursPos, DiffuseColours.Length);

            Array.Copy(DiffuseColours, 0, diffuseColours, diffuseColoursPos, DiffuseColours.Length);
            diffuseColoursPos += DiffuseColours.Length;

            if (Normals != null)
            {
                CheckAndExpandArray(ref normals, normalPos, Normals.Length);
                Array.Copy(Normals, 0, normals, normalPos, Normals.Length);
            }

            normalPos += Verts.Length;

            CheckAndExpandArray(ref tc, tcPos, TextureCoords.Length);

            Array.Copy(TextureCoords, 0, tc, tcPos, TextureCoords.Length);
            tcPos += TextureCoords.Length;

            if (MaterialIDs != null)
            {
                CheckAndExpandArray(ref materialIDs, materialIDPos, MaterialIDs.Length);
                Array.Copy(MaterialIDs, 0, materialIDs, materialIDPos, MaterialIDs.Length);
            }

            materialIDPos += Verts.Length;

            return this;
        }

        private static void CheckAndExpandArray<T>(ref T[] targetArray, Int32 copyToPosition, Int32 targetArraySize, Int32 additionalBuffer = 1024)
        {
            if (copyToPosition + targetArraySize <= targetArray.Length) return;
            T[] newVerts = new T[targetArray.Length + targetArraySize + additionalBuffer];
            Array.Copy(targetArray, newVerts, targetArray.Length);
            targetArray = newVerts;
        }
    }

    /// <summary>
    ///     Thrown when a renderable with an inbalance between the sizes of the colour,texture corrdinate and vert arrays
    /// </summary>
    internal class UnbalancedRenderableExcaption : Exception
    {
    }
}